CREATE TABLE  `product_retail_metrics` (
  `product_retail_metric_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) DEFAULT NULL,
  `metric_retail_id` int(10) DEFAULT NULL,
  `added_by` int(10) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  PRIMARY KEY (`product_retail_metric_id`),
  KEY `FK_product_retail_metrics_product_id` (`product_id`),
  KEY `FK_product_retail_metrics_metric` (`metric_retail_id`),
  KEY `FK_product_retail_metrics_userid` (`added_by`),
  CONSTRAINT `FK_product_retail_metrics_userid` FOREIGN KEY (`added_by`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_product_retail_metrics_metric` FOREIGN KEY (`metric_retail_id`) REFERENCES `metric_retail` (`metric_retail_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_product_retail_metrics_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE  `ctm`.`product_wholesale_metrics` (
  `product_wholesale_metric_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) DEFAULT NULL,
  `metric_wholesale_id` int(10) DEFAULT NULL,
  `added_by` int(10) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  PRIMARY KEY (`product_wholesale_metric_id`),
  KEY `FK_product_wholesale_metrics_product_id` (`product_id`),
  KEY `FK_product_wholesale_metrics_metric` (`metric_wholesale_id`),
  KEY `FK_product_wholesale_metrics_userid` (`added_by`),
  CONSTRAINT `FK_product_wholesale_metrics_userid` FOREIGN KEY (`added_by`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_product_wholesale_metrics_metric` FOREIGN KEY (`metric_wholesale_id`) REFERENCES `metric_wholesale` (`metric_wholesale_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_product_wholesale_metrics_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

