<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div class="row-fluid" style="margin-bottom: 20px;">
    <div class="span12">

        <div class="row-fluid">
            <div class="span3">
                <div class="row-fluid">
                    <ul class="tiles">
                        <li class="lime high long dashboard_icon img-circle">
                            <a href="<?= site_url('/report') ?>">
                                <span class="nopadding">
                                    <h5>Basic Report <br>
                                        <span style="font-size: 10px; padding-top: 10px;">
                                            <i>Daily prices by product type</i>
                                        </span>                                 
                                    </h5>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="span3">
                    <div class="row-fluid">
                        <ul class="tiles">
                            <li class="orange high long dashboard_icon img-circle">

                                <a href="<?= site_url('/analytics') ?>">
                                    <span class="nopadding">
                                        <h5>Wholesale Analytics</h5>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
            </div>
            <div class="span3">
                    <div class="row-fluid">
                        <ul class="tiles">
                            <li class="red high long dashboard_icon img-circle">
                                <a href="<?= site_url('/retail-analytics') ?>">
                                    <span class="nopadding">
                                        <h5>Retail Analytics</h5>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
            </div>
            <div class="span3">
                    <div class="row-fluid">
                        <ul class="tiles">
                            <li class="green high long dashboard_icon img-circle">
                                <a href="<?= site_url('/source-products') ?>">
                                    <span class="nopadding">
                                        <h5>Products by Source Analytics</h5>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
            </div>
        </div>

        <!--        <ul class="tiles" style="margin: auto; width: 50%; padding: 10px;">
                    <li class="lime high long dashboard_icon img-circle">
                        <a href="<?= site_url('/report') ?>">
                            <span class="nopadding">
                                <h5>Basic Report <br>
                                    <span style="font-size: 10px; padding-top: 10px;">
                                        <i>Daily prices by product type</i>
                                    </span>                                 
                                </h5>
                            </span>
                        </a>
                    </li>
                    <li class="orange high long dashboard_icon img-circle">
        
                        <a href="<?= site_url('/analytics') ?>">
                            <span class="nopadding">
                                <h5>Wholesale Analytics</h5>
                            </span>
                        </a>
                    </li>
                    <li class="red high long dashboard_icon img-circle">
                        <a href="<?= site_url('/retail-analytics') ?>">
                            <span class="nopadding">
                                <h5>Retail Analytics</h5>
                            </span>
                        </a>
                    </li>
                    <li class="green high long dashboard_icon img-circle">
                        <a href="<?= site_url('/source-products') ?>">
                            <span class="nopadding">
                                <h5>Products by Source Analytics</h5>
                            </span>
                        </a>
                    </li>
                </ul>-->
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <?php
        $market_str = '';
        if (!empty($markets)):
            foreach ($markets as $market):
                $market_str .= "{$market->market_name}, ";
            endforeach;
        endif;
        ?>
        <h3 class="lead" style="text-align: center; margin-bottom: 0px">Available Markets</h3>
        <div style="font-size: 1.0em; font-weight: bold; text-align: center">
            <?php echo rtrim($market_str, ', '); ?>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">

        <h3 class="lead" style="text-align: center; margin-bottom: 0px;">Available States</h3>
        <div style="font-size: 1.0em; font-weight: bold; text-align: center">
            Lagos  <sup>&Star;</sup><br>
            <sup>&Star;</sup> Other States coming soon
        </div>
    </div>
</div>
