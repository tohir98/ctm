<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>

<div id="navigation">
    <div class="container-fluid">
        <a href="<?php echo $dashboard_url; ?>" id="brand">COMPARE THE MARKETS <span style="font-size: 10px; font-weight: bold; color: #ffdf00">beta</span></a>
        <ul class='main-nav'>
            <li class=''>
                <a href="<?= site_url('/admin/dashboard') ?>">
                    <span>Dashboard</span>
                </a>
            </li>
            <?php if ($this->user_auth_lib->is_super_admin()): ?>
                <li>
                    <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                        <span>Administration</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?= site_url('admin/users') ?>">Users</a>
                        </li>
                        <li>
                            <a href="<?= site_url('admin/user_types') ?>">User Types</a>
                        </li>
                        <li>
                            <a href="<?= site_url('/subscription/subscribers') ?>">Subscribers</a>
                        </li>
                        <li>
                            <a href="<?= site_url('/access_control') ?>">Access Control</a>
                        </li>
                        <!--                        <li>
                                                    <a href="<?= site_url('user/view_user_logs') ?>">User Logs</a>
                                                </li>-->
                        <li>
                            <a href="<?= site_url('user/view_price_logs') ?>">Price Entry Logs</a>
                        </li>
                    </ul>
                </li>
            <?php endif; ?>
            <?php if ($this->user_auth_lib->have_perm('setup:categories')): ?>
                <li>
                    <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                        <span>Setup</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?= site_url('setup/categories') ?>">Categories</a>
                        </li>
                        <li>
                            <a href="<?= site_url('setup/sources') ?>">Sources</a>
                        </li>
                        <li>
                            <a href="<?= site_url('setup/varieties') ?>">Varieties</a>
                        </li>
                        <li>
                            <a href="<?= site_url('setup/wholesale_metric') ?>">Wholesale Metric</a>
                        </li>
                        <li>
                            <a href="<?= site_url('setup/retail_metric') ?>">Retail Metric</a>
                        </li>
                        <li>
                            <a href="<?= site_url('setup/markets') ?>">Markets</a>
                        </li>
                        <li>
                            <a href="<?= site_url('setup/products') ?>">Products</a>
                        </li>
                        <li>
                            <a href="<?= site_url('setup/remarks') ?>">Remarks</a>
                        </li>
                        <?php if ($this->user_auth_lib->have_perm('admin:traders')): ?>
                            <li><a href="<?= site_url('/setup/traders') ?>">Traders</a></li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>
            <?php if (!$this->user_auth_lib->is_subscriber()): ?>
                <li>
                    <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                        <span><?= $this->user_auth_lib->is_subscriber() ? 'Report' : "Market" ?></span>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <?php if ($this->user_auth_lib->have_perm('setup:products')): ?>
                            <li>
                                <a href="<?= site_url('/setup/products') ?>">Prices</a>
                            </li>
                        <?php endif; ?>
                        <?php if ($this->user_auth_lib->have_perm('market:pending_approval')): ?>
                            <li><a href="<?= site_url('/market/pending_approval') ?>">Pending Approvals</a></li>
                        <?php endif; ?>
                        <?php if ($this->user_auth_lib->have_perm('market:exemptions')): ?>
                            <li><a href="<?= site_url('/report/exemption') ?>">Exemption</a></li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>
            <li><a href="<?= site_url('/report') ?>">Reports &amp; Analytics</a></li>
            <?php if ($this->user_auth_lib->have_perm('setup:adverts')): ?>
                <li><a href="<?= site_url('/advert') ?>">Manage Ads</a></li>
            <?php endif; ?>
            <li>
                <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                    <span><?= ucfirst($display_name); ?></span>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu pull-right">
                    <li><a href="<?= site_url('/user/change_password') ?>">Change Password</a></li>
                    <?php if ($this->user_auth_lib->is_subscriber()): ?>
                        <li><a href="<?= site_url('/subscriber/account') ?>">My Account</a></li>
                    <?php endif; ?>
                    <li><a href="<?php echo $logout_url; ?>">Sign out</a></li>
                </ul>
            </li>

        </ul>
        <!--        <div class="user">
                    <div class="dropdown">
                        <a href="#" class='dropdown-toggle' data-toggle="dropdown"><?= ucfirst($display_name); ?> <img src="/img/demo/user-avatar.jpg" alt=""></a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="<?= site_url('/user/change_password') ?>">Change Password</a></li>
                            <li><a href="<?php echo $logout_url; ?>">Sign out</a></li>
                        </ul>
                    </div>
                </div>-->


        <div class="user" style="float: right;">
            <?php
            if (ENVIRONMENT == 'development'):
                if (!$this->user_auth_lib->is_super_admin() && !$this->user_auth_lib->is_subscriber()):
                    ?>
                    <ul class="main-nav" style="float: right;">
                        <li style="background: #2c5e7b; max-height: 40px;">
                            <a href="#" class='dropdown-toggle' data-toggle="dropdown" style="max-height: 20px;"> 

                                <span title="<?= $this->user_auth_lib->get('last_name') ?>" style="max-width: 100px;
                                      white-space: nowrap; overflow: hidden; display: inline-block;">
                                    <i class="glyphicon-shopping_bag" style="display: inline-block;"></i> Switch Market
                                </span>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <?php
                                if (!empty($user_markets)):
                                    foreach ($user_markets as $mkt):
                                        ?>
                                        <li>
                                            <a href="#">
                                                <?= $mkt->market_name ?>
                                            </a>
                                        </li>
                                        <?php
                                    endforeach;
                                endif;
                                ?>

                            </ul>
                        </li>
                    </ul>
                <?php endif; ?>
            <?php endif; ?>
        </div>


    </div>
</div>
