<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
?>
<div id="footer" style="">
    <p>
        &copy; Compare the Markets Nigeria Ltd. <?= date('Y') ?>. All rights reserved. 
        
        <span class="font-grey-4">|</span> <a>Terms of service</a>
    </p>
</div>